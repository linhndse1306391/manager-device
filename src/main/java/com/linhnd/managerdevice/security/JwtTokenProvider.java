package com.linhnd.managerdevice.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;


@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);
    @Autowired
    private Key key;
    @Value("${app.accessTokenExpirationInMs}")
    private int accessTokenExpirationInMs;

    public String generateAccessToken(String email) {

        Date now = new Date();
        Date expiredDate = new Date(now.getTime() + accessTokenExpirationInMs);

        JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(email)
                .setIssuedAt(now)
                .setExpiration(expiredDate);
        return jwtBuilder.signWith(key).compact();

    }

    public String validateToken(String token) {
         String email = null;
        try {
            email = Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();

        } catch (ExpiredJwtException e) {
            logger.error("ExpiredJwt: " + e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("UnsupportedJwt: " + e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("MalformedJwt: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("IllegalArgument: " + e.getMessage());
        } catch (SignatureException e) {
            logger.error("Signature: " + e.getMessage());
        }

        return email;
    }


}
