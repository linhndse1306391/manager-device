package com.linhnd.managerdevice.security;

import com.linhnd.managerdevice.models.database.Users;
import com.linhnd.managerdevice.services.UserPrincipal;
import com.linhnd.managerdevice.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    private UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {


        try {
            String jwtToken = getJwtFromToken(httpServletRequest);

            if (!StringUtils.hasText(jwtToken)) {
                logger.error("Could not set user authentication in security context No token");
            }

            String email = tokenProvider.validateToken(jwtToken);
            if (email != null) {
                Users user = userService.getUserbyEmail(tokenProvider.validateToken(email));

                UserDetails userPrincipal = UserPrincipal.create(user);
                SecurityContext securityContext = SecurityContextHolder.getContext();
                Authentication auth = securityContext.getAuthentication();

                if (auth == null || auth instanceof AnonymousAuthenticationToken) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                            userPrincipal,
                            null,
                            userPrincipal.getAuthorities()
                    );
                    authentication.setDetails(
                            new WebAuthenticationDetailsSource()
                                    .buildDetails(httpServletRequest)
                    );
                    securityContext.setAuthentication(authentication);
                }
            }
        } catch (Exception ex) {
            logger.error("Could not set user authentication in security context {}", ex.getMessage());
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getJwtFromToken(HttpServletRequest request) {
        String bearToken = request.getHeader("Authorization");

        String jwt = null;
        if (StringUtils.hasText(bearToken) && bearToken.startsWith("Bearer")) {
            jwt = bearToken.substring(7);
        }
        return jwt;
    }


}
