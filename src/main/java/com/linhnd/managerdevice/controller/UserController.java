package com.linhnd.managerdevice.controller;


import com.linhnd.managerdevice.config.ApiResponse;
import com.linhnd.managerdevice.config.ApiResponseList;
import com.linhnd.managerdevice.models.database.Users;
import com.linhnd.managerdevice.models.rest.UserLogin;
import com.linhnd.managerdevice.security.JwtTokenProvider;
import com.linhnd.managerdevice.services.UserPrincipal;
import com.linhnd.managerdevice.services.UserServiceIml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserServiceIml userServiceIml;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping
    @RequestMapping("/login")
    public ResponseEntity getLogin(@RequestBody UserLogin userLogin) {
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userLogin.getUsername(), userLogin.getPassword())
        );

        String userId = ((UserPrincipal) auth.getPrincipal()).getId();
        // ham lay thong tin user
        Users users = userServiceIml.getUserbyEmail(userId);

        String jwt = jwtTokenProvider.generateAccessToken(users.getEmail());
        // Oggy here
        return ResponseEntity.ok(jwt);

//        UserResponse userResponse = userServiceIml.getLogin(userLogin);
//        if(userResponse != null){
//            return ApiResponse.builder().statusCode(200).status("success").result(userResponse).build();
//        }else{
//            return  ApiResponse.builder().statusCode(100).status("failed").result("failed").build();
//        }
    }


    @GetMapping
    @RequestMapping("/all")
    public ApiResponseList getAll() {
        List all = userServiceIml.getAll();
        return ApiResponseList.builder().statusCode(200).status("success").result(all).build();
    }

    @GetMapping
    @RequestMapping("/test")
    public ApiResponse getString() {
        return ApiResponse.builder().statusCode(200).status("success").result("ahihihi").build();
    }

    @GetMapping
    @RequestMapping("/delete/{id}")
    public Long hello(@PathVariable Long id) {
        return id;
    }


}
