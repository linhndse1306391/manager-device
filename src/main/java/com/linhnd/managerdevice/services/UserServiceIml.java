package com.linhnd.managerdevice.services;


import com.linhnd.managerdevice.models.database.Users;
import com.linhnd.managerdevice.repositorys.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceIml implements UserService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails findOne(String email) {
        Users user = new Users();
        user.setEmail(email);
        return UserPrincipal.create(user);
    }

    @Override
    public List<Users> getAll() {
        List<Users> all = usersRepository.findAll();
        return all;
    }

    @Override
    public Users getUserbyEmail(String email) {
        return usersRepository.findByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = usersRepository.findByEmail(username);
        return UserPrincipal.create(user);
    }


//    @Autowired
//    private UsersRepository usersRepository;
//
//
//    public UserResponse getLogin(UserLogin userLogin) {
//        Users users = usersRepository.getLogin(userLogin.getUsername(), userLogin.getPassword());
//        if (users == null)
//            return null;
//        return UserResponse.builder().name(users.getName()).role(users.getRoleId()).build();
//
//    }
//
//    public List getAll() {
//        List<Users> all = usersRepository.findAll();
//        return all;
//    }
}
