package com.linhnd.managerdevice.services;

import com.linhnd.managerdevice.models.database.Users;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    UserDetails findOne(String email);
    List<Users> getAll();
    Users getUserbyEmail(String email);

}
