package com.linhnd.managerdevice.services;

import com.linhnd.managerdevice.models.database.Users;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data

public class UserPrincipal implements UserDetails {

    private String id;
    private List<GrantedAuthority> grantedAuthorities;
    private String password;

    public UserPrincipal(String id, List<GrantedAuthority> grantedAuthorities, String password) {
        this.id = id;
        this.grantedAuthorities = grantedAuthorities;
        this.password = password;
    }

    public static UserPrincipal create(Users user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getName()));
        return new UserPrincipal(
                user.getEmail(),
                authorities,
                user.getPassword()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return this.getPassword();
    }

    @Override
    public String getUsername() {
        return id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
